(function (angular) {
    var mod = angular.module('mytree', []);
    mod.directive('myTree', function() {
        return {
            restrict: 'E',
            templateUrl: '/app/mytree.template.html',
            scope: {
                'id': '@',
                'text': '@'
            },
            bindToController: true,
            controllerAs: 'vm',
            controller: function(treeService) {
                var vm = this;
                vm.expanded = false;
                console.log('controller', vm.id, vm.text);
                vm.collapse = function() {
                    vm.expanded = false;
                };
                vm.expand = function() {
                    console.log('expand', vm.id, vm.text);
                    treeService
                    .getChildren(vm.id)
                    .then(function(resp) {
                        vm.children = resp.data;
                    });

                    vm.expanded = true;
                };
            },
            link: function (scope, element, attrs) {

            }
        };
    });

    mod.factory('treeService', function($http) {
        return {
            getChildren: function(pid) {
                return $http.get ('/api/children?pid=' + pid);
            }
        };
    });

})(angular);