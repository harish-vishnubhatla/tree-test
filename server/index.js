var express = require('express');
var path = require('path');
var app = express();
var staticHandler = express.static(path.resolve(__dirname, '..', 'public'), {
    index: 'index.html',
    redirect: true
});

var apiRouter = express.Router();
apiRouter.get('/children', function(req, res) {
    var parentId = req.query.pid || '0';
    console.log('Parent Id:', parentId);
    var children = [];
    for( var idx =0; idx< 10 ;idx++) {
        children.push ({
            id: parentId + '-' + idx,
            text: parentId + '-' + idx
        });
    }
    setTimeout(function() {
        return res.json(children);
    }, 800);
});

app.use('/api', apiRouter);
app.use(staticHandler);
app.listen(8080, function() {
    console.log('Listening..');
});

